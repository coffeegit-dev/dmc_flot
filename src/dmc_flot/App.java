/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dmc_flot;

import dmc_flot.util.Config;
import dmc_flot.views.VContainer;
import dmc_flot.views.VLogin;
import javax.swing.UIManager;

/**
 *
 * @author Sigit Prayitno <sigit.prayitno@service-division.com>
 */
public class App {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(
            UIManager.getSystemLookAndFeelClassName());
            UIManager.put("FileChooser.noPlacesBar", Boolean.TRUE);
//            UIManager.put("FileChooser.readOnly", Boolean.TRUE);
//            UIManager.put("FileChooser.upFolderIcon", Boolean.FALSE);
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            System.err.println("Error: "+ex.getMessage());
        }
        
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                Config config = new Config();
                if(config.canRunApp()){
                    new VLogin(null, true).setVisible(true);
                }else{
                    System.exit(0);
                }
            }
        });
    }
    
}
