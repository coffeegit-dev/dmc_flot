/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dmc_flot.views;

import dmc_flot.util.CSVParser;
import dmc_flot.util.Config;
import dmc_flot.util.FileFinder;
import dmc_flot.util.FileMatcher;
import dmc_flot.util.SuffixMatcher;
import dmc_flot.util.TemplateReader;
import dmc_flot.util.Util;
import dmc_flot.util.Wording;
import dmc_flot.widgets.MessageConsole;
import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author Sigit Prayitno <sigit.prayitno@service-division.com>
 */
public class VOutputModifier extends javax.swing.JInternalFrame {
    private static VOutputModifier myInstance;
    String version = Wording.version_modifier;
    
    private static final boolean IS_WINDOWS = System.getProperty( "os.name" ).contains( "indow" );
    public static final String NL = System.getProperty("line.separator");
    public static final String FL = System.getProperty("file.separator");
    
    Config config = new Config();
    TemplateReader templateReader = new TemplateReader();
    Util utility = new Util();
    
    String template_path = config.getTemplate_path();
    String load_project = null;
    String root_path = null;
    String init_path = null;
    String source_root_dir = null;
    String target_root_dir = null;
    
    List<String> csv_list = new ArrayList<>();
    List<String> tiff_list = new ArrayList<>();
    List<String> mapping_dest_list = new ArrayList<>();
    List<String> lookup_tables = new ArrayList<>();
    
    int totalCSV;
    
    /**
     * Creates new form VOutputModifier
     */
    public VOutputModifier() {
        initComponents();
        setTitle("Release Directory Modifier Version "+version);
        // add widget MessageConsole to write output to textpane
//        MessageConsole mc = new MessageConsole(txtPaneLogs);
//        mc.redirectOut();
//        mc.redirectErr(Color.RED, null);
//        mc.setMessageLines(200);
    }
    
    public static VOutputModifier getInstance() {
        if (myInstance == null) {
            myInstance = new VOutputModifier();
        }
        return myInstance;
    }
    
    DefaultMutableTreeNode createTree(File temp)  
    {  
        DefaultMutableTreeNode top=new DefaultMutableTreeNode(temp.getPath());  
        if(!(temp.exists() && temp.isDirectory()))  
            return top;  

        fillTree(top,temp.getPath());  

        return top;  
    }
    
    private void fillTree(DefaultMutableTreeNode root, String filename)  
    {  
        File temp = new File(filename);  

        if(!(temp.exists() && temp.isDirectory()))  
            return;  
        File[] filelist = temp.listFiles();  

        for(int i=0; i < filelist.length; i++)  
        {
            if(!filelist[i].isDirectory()){
                continue;  
            }else{
                final DefaultMutableTreeNode rootNode = new DefaultMutableTreeNode(filelist[i].getName());  
                root.add(rootNode);  
                final String newfilename = new String(filename+"\\"+filelist[i].getName());  
                Thread t = new Thread()  
                {  
                    @Override
                    public void run()  
                    {  
                        fillTree(rootNode, newfilename);  
                    }//run  
                };//thread  
                if(t == null)   
                {
                    System.out.println("no more thread allowed "+newfilename);return;
                }  
                t.start();  
            }
        }//for  
    }//function
    
    private void actLoadProject(){
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setCurrentDirectory(new File(template_path));
        fileChooser.setDialogTitle("Find project ini file");
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        //
        // only accept ini or txt file.
        //
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Text files (*.txt, *.ini)", "txt", "ini");
        fileChooser.setFileFilter(filter);
        //    
        if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) { 
            String location = fileChooser.getSelectedFile().getPath();
            String filename = fileChooser.getSelectedFile().getName();
            load_project = location;
            txtLoadProject.setText(filename);
            
            ArrayList val_content = new ArrayList();
            ArrayList val_sub_content = new ArrayList();
//            String filename = txtLoadProject.getText();

                String[] lines = templateReader.readINI(load_project);
                for (String line : lines) 
                {
                    val_content.clear();
                    val_content = utility.getFileContent(line, "=");
                    String field = String.valueOf(val_content.get(0).toString());
                    String content = String.valueOf(val_content.get(1).toString());
                    if(field.equals("rootdir")){
                        if( content.isEmpty() ){
                            root_path = ".";
                        }else{
                            root_path = content;
                        }
                    }
                    if(field.equals("last_saved_source")){
                        if( content.isEmpty() ){
                            init_path = "-";
                        }else{
                            init_path = content;
                        }
                    }
                    if(field.equals("source_root_dir")){
                        if( content.isEmpty() ){
                            source_root_dir = "";
                        }else{
                            source_root_dir = content;
                        }
                    }
                    if(field.equals("target_root_dir")){
                        if( content.isEmpty() ){
                            target_root_dir = "";
                        }else{
                            target_root_dir = content;
                        }
                    }
                    if(field.equals("lookuptable")){
                        if( content.isEmpty() ){
                            lookup_tables = null;
                        }else{
                            val_sub_content.clear();
                            val_sub_content = utility.getFileContent(content, ";");
                            String f_name = String.valueOf(val_sub_content.get(0).toString());
                            String f_path = String.valueOf(val_sub_content.get(1).toString());
                            String[] csv_lines = templateReader.readTemplate(f_path);
                            
                            int c = 0;
                            for (String r : csv_lines) {
                                c++;
//                                System.out.println("Field "+c+" :"+r);
                                lookup_tables.add(r);
                            }
                        }
                    }
                    if(field.equals("mapping_dest")){
                        if( content.isEmpty() ){
                            mapping_dest_list = null;
                        }else{
                            val_sub_content.clear();
                            val_sub_content = utility.getSplitSingleData(content, ";");
                            Iterator<String> itr = val_sub_content.iterator();
                            while (itr.hasNext()) {
                                String element = itr.next();
//                                System.out.println("element:"+element);
                                mapping_dest_list.add(element);
                            }
                        }
                    }
                }
                lbl_init.setText(init_path);

//                findCSV(lbl_init.getText());
                searchCSV(new File(lbl_init.getText()));
        }
        else {
            System.out.println("No Selection ");
        }
    }
    
    private void actBtnSrc(){
        JFileChooser fileChooser = new JFileChooser();
        if(init_path.equals("-")){
            fileChooser.setCurrentDirectory(new File(root_path));
        }else{
            fileChooser.setCurrentDirectory(new File(init_path));
        }
        fileChooser.setDialogTitle("Find source directory");
        fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        //
        // disable the "All files" option.
        //
        fileChooser.setAcceptAllFileFilterUsed(false);
        //    
        if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) { 
            String location = fileChooser.getSelectedFile().getPath().toString() + FL;
            txtSrc.setText(location);
            File temp=new File(location);
            DefaultMutableTreeNode rootNode = createTree(temp);
//            DefaultMutableTreeNode rootNode = new DefaultMutableTreeNode(location);
            DefaultTreeModel dTree = new DefaultTreeModel(rootNode);
//            treeOldDir.setModel(dTree);
        }
        else {
            System.out.println("No Selection ");
        }
    }
    
    private void actBtnTarget(){
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setCurrentDirectory(new File(root_path));
        if(root_path == null){
            fileChooser.setCurrentDirectory(fileChooser.getFileSystemView().getParentDirectory(new File("C:\\")));
        }else{
            fileChooser.setCurrentDirectory(new File(root_path));
        }
        fileChooser.setDialogTitle("Find target directory");
        fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        //
        // disable the "All files" option.
        //
        fileChooser.setAcceptAllFileFilterUsed(false);
        //    
        if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) { 
            String location = fileChooser.getSelectedFile().getPath().toString() + FL;
            txtTarget.setText(location);
            File temp=new File(location);
            DefaultMutableTreeNode rootNode = createTree(temp);
//            DefaultMutableTreeNode rootNode = new DefaultMutableTreeNode(location);
            DefaultTreeModel dTree = new DefaultTreeModel(rootNode);
//            treeOldDir.setModel(dTree);
        }
        else {
            System.out.println("No Selection ");
        }
    }
    
    private void actBtnDown(){
        String source;
        String target;
        File file;
        
        source = txtSrc.getText();
        if(source.isEmpty() || source.equals("")){
            JOptionPane.showMessageDialog(null, "Source location cannot be null!", 
                    "Message", JOptionPane.WARNING_MESSAGE);
        }else{
            txtTarget.setText(source.replaceAll(source_root_dir, target_root_dir));
            target = txtTarget.getText();
            file = new File(target);
            if(!file.exists()){
                file.mkdirs();
                System.out.println("Directory created -> "+file.getAbsolutePath());
            }else{
                System.out.println("Directory already exist!");
            }
        }
    }
    
    private void findCSV(String src) {
        File folder = new File(src);
        String filename = "";
        
        for (File fileEntry : folder.listFiles()) {
            if (fileEntry.isDirectory()) {
                findCSV(fileEntry.getAbsolutePath().toString());
            }else {
                if (fileEntry.isFile()) {
                    filename = fileEntry.getName().toString();
                    if(filename.endsWith(".csv")){
                        String csv_files = folder.getAbsolutePath()+ "\\" + fileEntry.getName();
                        csv_list.add(csv_files);
                        totalCSV++;
                    }
                }
            }
        }
    }    
    
    private void searchCSV(File src){
        FileMatcher matcher = new SuffixMatcher(".csv");
        FileFinder finder = new FileFinder(matcher, src);
        try {
            
            for (File source : finder.findMatchingFiles()) {
                csv_list.add(source.getAbsolutePath().toString());
            }
        } catch (IOException ex) {
            System.out.println("Error:"+ex.getMessage());
        }
    }
    
    private void searchTIFF(File src){
        FileMatcher matcher = new SuffixMatcher(".tif");
        FileFinder finder = new FileFinder(matcher, src);
        try {
            
            for (File source : finder.findMatchingFiles()) {
                tiff_list.add(source.getAbsolutePath().toString());
            }
        } catch (IOException ex) {
            System.out.println("Error:"+ex.getMessage());
        }
    }
    
    private void processData() {
        File csv_file;
        File csv_target_file;
        String csv_parent_path;
        String csv_target_path;
        String target_root_path;
        for(int i = 0; i < csv_list.size();i++){
//            System.out.println("CSV File:"+csv_list.get(i));
            CSVParser parser;
            parser = new CSVParser(this);
            System.out.println("Reading csv:"+csv_list.get(i));
//            parser.readCSVFile(csv_list.get(i).toString(), lookup_tables, mapping_dest_list);

            List<String> csv_contents = new ArrayList<>();
            csv_file = new File(csv_list.get(i));
            csv_parent_path = csv_file.getParent();
            csv_target_path = csv_list.get(i).replaceFirst(source_root_dir, target_root_dir);
            csv_target_file = new File(csv_target_path);
//            new_parent_path = parent_path.replaceFirst("OUTPUT", "NEW_OUTPUT");
//            target_root_path = txtTarget.getText().substring(-1); // remove path string last slash
            target_root_path = txtTarget.getText();
            csv_contents = parser.getCsvContent(csv_list.get(i));
            
            try {
                FileUtils.copyFile(csv_file, csv_target_file);
            } catch (IOException ex) {
                System.err.println("Error:"+ex.getMessage());
            }
            
            int x = 0;
            for(String csv_content : csv_contents){
//                parser.extractCSV(s, lookup_tables, mapping_dest_list);
                System.out.println("csv_parent_path:"+csv_parent_path);
                System.out.println("csv_target_path:"+csv_target_path);
                System.out.println("target_root_path:"+target_root_path);
//                utility.extractMapping(parser.extractCSV(csv_content, lookup_tables, mapping_dest_list), target_root_path, tiff_list.get(x));
                System.out.println("---------------------------------------------");
                x++;
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtSrc = new javax.swing.JTextField();
        btnSrc = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        txtLoadProject = new javax.swing.JTextField();
        btnLoadProject = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        lbl_init = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txtTarget = new javax.swing.JTextField();
        btnTarget = new javax.swing.JButton();
        btnProcess = new javax.swing.JButton();
        btnDown = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtPaneLogs = new javax.swing.JTextPane();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Source Directory Location"));

        jLabel1.setText("Source Location :");

        txtSrc.setEditable(false);
        txtSrc.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        btnSrc.setText("...");
        btnSrc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSrcActionPerformed(evt);
            }
        });

        jLabel5.setText("Load Project :");

        txtLoadProject.setEditable(false);
        txtLoadProject.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        btnLoadProject.setText("...");
        btnLoadProject.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLoadProjectActionPerformed(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel6.setText("Last saved source location :");

        lbl_init.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_init.setText("-");

        jLabel7.setText("Target Location :");

        txtTarget.setEditable(false);
        txtTarget.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        btnTarget.setText("...");
        btnTarget.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTargetActionPerformed(evt);
            }
        });

        btnProcess.setText("Process");
        btnProcess.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnProcessActionPerformed(evt);
            }
        });

        btnDown.setIcon(new javax.swing.ImageIcon(getClass().getResource("/dmc_flot/assets/arrows.png"))); // NOI18N
        btnDown.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDownActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lbl_init, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(txtLoadProject, javax.swing.GroupLayout.PREFERRED_SIZE, 168, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnLoadProject)
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(btnProcess)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(txtTarget, javax.swing.GroupLayout.DEFAULT_SIZE, 624, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnTarget))))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtSrc, javax.swing.GroupLayout.DEFAULT_SIZE, 623, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(btnDown, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                            .addComponent(btnSrc, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabel1, jLabel5});

        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txtLoadProject, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnLoadProject))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(lbl_init, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtSrc, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSrc))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnDown)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(txtTarget, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnTarget))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnProcess, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("Steps");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel3.setText("1. Load project modifier .ini file");

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel4.setText("2. Set initial directory location");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel4)
                .addContainerGap(394, Short.MAX_VALUE))
        );

        jScrollPane1.setBorder(javax.swing.BorderFactory.createTitledBorder("Stream Output"));
        jScrollPane1.setViewportView(txtPaneLogs);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 252, Short.MAX_VALUE)))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnSrcActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSrcActionPerformed
        actBtnSrc();
    }//GEN-LAST:event_btnSrcActionPerformed

    private void btnLoadProjectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLoadProjectActionPerformed
        actLoadProject();
    }//GEN-LAST:event_btnLoadProjectActionPerformed

    private void btnTargetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTargetActionPerformed
        actBtnTarget();
    }//GEN-LAST:event_btnTargetActionPerformed

    private void btnDownActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDownActionPerformed
        actBtnDown();
    }//GEN-LAST:event_btnDownActionPerformed

    private void btnProcessActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnProcessActionPerformed
        processData();
    }//GEN-LAST:event_btnProcessActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnDown;
    private javax.swing.JButton btnLoadProject;
    private javax.swing.JButton btnProcess;
    private javax.swing.JButton btnSrc;
    private javax.swing.JButton btnTarget;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel lbl_init;
    public javax.swing.JTextField txtLoadProject;
    private javax.swing.JTextPane txtPaneLogs;
    private javax.swing.JTextField txtSrc;
    private javax.swing.JTextField txtTarget;
    // End of variables declaration//GEN-END:variables
}
