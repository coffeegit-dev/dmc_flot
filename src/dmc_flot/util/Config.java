/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dmc_flot.util;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import javax.swing.JOptionPane;

/**
 *
 * @author Sigit Prayitno <sigit.prayitno@service-division.com>
 */
public class Config {
    private static Connection conn = null;
    
    private final String root_dir = "D:";
    private String template_path;
    
    public static Connection connect_db(){
        String path_db = "program-db/digi_tis.db";
        
        try{
            Class.forName("org.sqlite.JDBC"); 
            File temp = new File(path_db);
            if(temp.exists()){
                String connection = "jdbc:sqlite:" + temp.getAbsolutePath().replace("\\","\\\\");
                conn = DriverManager.getConnection(connection);
//                JOptionPane.showMessageDialog(null, "Connected to DB!", null, JOptionPane.INFORMATION_MESSAGE);
            }else{
                JOptionPane.showMessageDialog(null, "DB file not found!", null, JOptionPane.INFORMATION_MESSAGE);
                System.exit(0);
            }

        }catch(Exception e){
            JOptionPane.showMessageDialog(null, e.getMessage(), null, JOptionPane.ERROR_MESSAGE); 
        }
        return conn; 
    }

    public String getTemplate_path() {
        template_path = root_dir + System.getProperty("file.separator") + "template";
        File dir = new File(template_path);
        if(dir.exists()){
            if(dir.isDirectory()){
                template_path = template_path;
            }
        }else{
            template_path = "";
        }
        return template_path;
    }
    
    public boolean canRunApp(){
        if(getTemplate_path().isEmpty()){
            JOptionPane.showMessageDialog(null, "Template folder not found! \nPlease create folder template in Directory "+root_dir, 
                    "Message", JOptionPane.INFORMATION_MESSAGE);
            return false;
        }else{
            return true;
        }
    }
}
