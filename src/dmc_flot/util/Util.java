/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dmc_flot.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;

/**
 * Utility class to be used internally by the formatter.
 * 
 * @author Sigit Prayitno <sigit.prayitno@service-division.com>
 */
public class Util {
    public static final String NL = System.getProperty("line.separator");
    public static final String FL = System.getProperty("file.separator");
    
    public static String createSpacer(int length) {
        return createString(" ", length);
    }
    
    public static String createString(String chr, int length) {
        if (length <= 0) {
            return "";
        }
        
        StringBuilder sb = new StringBuilder();
        while(sb.length() < length) {
            sb.append(chr);
        }
        
        return sb.toString().substring(0, length);
    }
    
    /**
     * Checks the specified object for not null and throws IllegalArgumentException if it is
     * null. Message reads <code>The parameter {name} cannot be null</code>
     * 
     * @param object - object to be checked for not-null
     * @param name - parameter name to be included in the message
     * 
     * @throws NullPointerException if object is null
     */
    public static void checkForNotNull(Object object, String name) {
        if (object == null) {
            throw new IllegalArgumentException("The parameter '" + name + "' cannot be null");
        }
    }
    
    public ArrayList getFileContent(String text, String delimiter) {
        ArrayList valArray = new ArrayList();

        String[] values = text.split(delimiter, -1);
        valArray.add(0, values[0]);
        if(values[1].isEmpty()){
            valArray.add(1, "");
        }else{
            valArray.add(1, values[1]);
        }
        return valArray;
    }
    
    public ArrayList getSplitSingleData(String text, String delimiter) {
        ArrayList valArray = new ArrayList();

        String[] values = text.split(delimiter, -1);
        for(String v : values){
            valArray.add(v);
        }
        return valArray;
    }
    
    public boolean extractMapping(String mapping, String parent_path, String tiff_path){
        boolean accepted = false;
        StringBuilder sb = new StringBuilder();
        File file;
        File file_tiff;
        String parent_tiff;
        String output_dir;
        
        String[] values = mapping.split(";");
        for(String v : values){
            sb.append(v).append(FL);
        }
        output_dir = parent_path + FL + sb.toString();
        file = new File(output_dir);
        if(!file.exists()){
            file.mkdirs();
            System.out.println("Images Directory Created -> " + output_dir);
            file_tiff = new File(tiff_path);
            parent_tiff = file_tiff.getParent();
//            try {
//                FileUtils.copyDirectoryToDirectory(new File(parent_tiff), new File(output_dir));
//                System.out.println("Images Copied -> "+parent_tiff+ " to " + output_dir);
//            } catch (IOException ex) {
//                System.err.println("Error:"+ex.getMessage());
//            }
        }else{
            System.out.println("Images Directory already exist!");
        }
        
        return accepted;
    }
    
    public boolean copyTiffFolder(String filePath){
        boolean success = false;
            String tiffName = null;
            String newTiffName = null;
            String dirImg = null;
            String dirPrefix = null;      
            File dirTiff = new File(filePath);
            tiffName = dirTiff.getName();
            dirImg = dirTiff.getParent();

            File pathdirImg = new File(dirImg);
            String[] names = pathdirImg.list();
            String SDir = null;
            String CDir = null;
            
            for(String name : names)
            {
                SDir = dirImg + "\\" + name + "\\S";
                CDir = dirImg + "\\" + name + "\\C";
                if(name.contains("WINCOR")){
                    if (new File(SDir).isDirectory() && new File(SDir).exists())
                    {
                        dirPrefix = SDir;
                    }
                    if (new File(CDir).isDirectory() && new File(CDir).exists())
                    {
                        dirPrefix = CDir;
                    }
                }
            }
            File sourceFolder = null;
            File sourceFolder1 = null;
            File targetFolder = null;
            File targetFolder1 = null;
            sourceFolder = new File(dirPrefix);
//            targetFolder = new File(targetDirectory);
//            FileUtils.copyDirectoryToDirectory(sourceFolder, targetFolder);
            System.out.println("Copying Image: to "+targetFolder.toString());
            success = true;
        return success;
    }
}
