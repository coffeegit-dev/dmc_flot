/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dmc_flot.util;

import java.awt.Component;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Sigit Prayitno <sigit.prayitno@service-division.com>
 */
public class CSVParser {
    Component frame;
    Util utility = new Util();
    public static final String NL = System.getProperty("line.separator");
    public static final String FL = System.getProperty("file.separator");
    
    public CSVParser(Component frame) {
        this.frame = frame;
    }
    
    public String readCSVFile(String filename, List lookup_table, List mapping_dest) {
        StringBuilder cleanCSV = new StringBuilder();
        BufferedReader fileReader = null;
        //Delimiter used in CSV file
        final String DELIMITER = "|";
        
        try {
            fileReader = new BufferedReader( new FileReader(filename) );
            String line = null;
            StringTokenizer token = null;
            int lineNum = 0, tokenNum = 0, subTokenNum = 0;
            File filenames = null;
            filenames = new File(filename);
            String namafile = null;
            namafile = filenames.getName();
            
            while((line = fileReader.readLine()) != null) {
                lineNum++;
                if(lineNum > 1){
                    token = new StringTokenizer(line, DELIMITER);
                    token.setReturnEmptyTokens(true);
                    while(token.hasMoreTokens()) {
                        tokenNum++;
                        String nextToken = token.nextToken();
                        System.out.println("nextToken:"+nextToken+" at lineNum: "+lineNum+" at tokenNum: "+tokenNum);
                        
//                        int current = tokenNum - 1;
//                        int next = tokenNum + 1;
//                        String header = lookup_table.get(current).toString();
//                        System.out.println("lookup_table_size:"+lookup_table.size());
//                        System.err.println("MAPPING "+tokenNum+" - "+header+": "+nextToken);
//                        System.out.println(Arrays.asList(mapping_dest));

                        if(tokenNum >= lookup_table.size()){
                            subTokenNum = tokenNum - lookup_table.size();
                            System.err.println("RENEW TOKEN: "+subTokenNum+" at "+tokenNum);
                        }

                        if(compare(lookup_table, mapping_dest)){
                            System.err.println("MAPPING: "+nextToken);
                        }

                    }
                    System.out.println();
                }
            }
        } catch (FileNotFoundException ex) {
            System.err.println("Error "+ex.getMessage());
        } catch (IOException ex) {
            System.err.println("Error "+ex.getMessage());
        } finally {
            try {
                fileReader.close();
            } catch (IOException ex) {
                System.err.println("Error "+ex.getMessage());
            }
        }
        return cleanCSV.toString();
    }
    
    public List getCsvContent(String filename) {
        List<String> csv_list = new ArrayList<>();
        BufferedReader fileReader = null;
        //Delimiter used in CSV file
        final String DELIMITER = "|";
        
        try {
            fileReader = new BufferedReader( new FileReader(filename) );
            String line = null;
            StringTokenizer token = null;
            int lineNum = 0, indexNum = 0, tokenNum = 0, subTokenNum = 0;
            File filenames = null;
            filenames = new File(filename);
            String namafile = null;
            namafile = filenames.getName();
            
            while((line = fileReader.readLine()) != null) {
                lineNum++;
                if(lineNum > 1){
                    csv_list.add(indexNum, line);
                }
            }
        } catch (FileNotFoundException ex) {
            System.err.println("Error "+ex.getMessage());
        } catch (IOException ex) {
            System.err.println("Error "+ex.getMessage());
        } finally {
            try {
                fileReader.close();
            } catch (IOException ex) {
                System.err.println("Error "+ex.getMessage());
            }
        }
        return csv_list;
    }
    
    public String extractCSV(String text, List lookup_table, List mapping_dest){
        StringBuilder cleanCSV = new StringBuilder();
        //Delimiter used in CSV file
        final String DELIMITER = "|";
        StringTokenizer token = null;
        int tokenNum = 0, subTokenNum = 0;
        
        token = new StringTokenizer(text, DELIMITER);
        token.setReturnEmptyTokens(true);
        
        int header_limit = lookup_table.size();
        int token_size = token.countTokens();
        
        if(token_size > header_limit){
            JOptionPane.showMessageDialog(this.frame, "Header limit size exceed! \nplease renew this header template!", 
                    "Operation cancelled by system!", JOptionPane.WARNING_MESSAGE);
            System.exit(0);
        }else if(token_size == header_limit){      
            while(token.hasMoreTokens()) {
                tokenNum++;
                String nextToken = token.nextToken();
    //            System.out.println("nextToken:"+nextToken+" at tokenNum: "+tokenNum);

                int current = tokenNum - 1;
                int next = tokenNum + 1;
                String header = lookup_table.get(current).toString();
//                System.err.println("VAR_DUMP "+tokenNum+" - "+header+": "+nextToken);
                
                if(mapping_dest.contains(header)){
                    System.out.println("MAPPING:"+header+" -> "+nextToken.replaceAll(" ", "-"));
                    cleanCSV.append(nextToken.replaceAll(" ", "-")+";");
                }
            }
        }else{
            JOptionPane.showMessageDialog(this.frame, "Unknown Error!", 
                    "Operation cancelled by system!", JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        }
        
        return cleanCSV.toString();
    }
    
    private boolean compare(List<String[]> list1, List<String[]> list2) 
    {
        return Arrays.deepEquals(list1.toArray(), list2.toArray());
    }
}
