/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dmc_flot.util;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

/**
 *
 * @author Sigit Prayitno <sigit.prayitno@service-division.com>
 */
public class FilesContentProvider implements TreeModel{
    private File node;

    public FilesContentProvider(String path) {
        node = new File(path);
    }

    @Override
    public Object getRoot() {
        return node;
    }

    @Override
    public Object getChild(Object parent, int index) {
        if (parent == null)
        return null;
        return ((File) parent).listFiles()[index];
    }

    @Override
    public int getChildCount(Object parent) {
        if (parent == null)
        return 0;
        return (((File) parent).listFiles() != null) ? ((File) parent).listFiles().length : 0;
    }

    @Override
    public boolean isLeaf(Object node) {
        return ((File) node).isFile();
//        return ((File) node).isDirectory();
    }

    @Override
    public void valueForPathChanged(TreePath path, Object newValue) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getIndexOfChild(Object parent, Object child) {
        List<File> list = Arrays.asList(((File) parent).listFiles());
        return list.indexOf(child);
    }

    @Override
    public void addTreeModelListener(TreeModelListener l) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void removeTreeModelListener(TreeModelListener l) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
