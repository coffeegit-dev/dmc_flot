/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dmc_flot.util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Sigit Prayitno <sigit.prayitno@service-division.com>
 */
public class TemplateReader {
    private String ini_file;
    
    public String[] readINI(String filename) 
    {
        FileReader fileReader = null;
        List<String> lines = new ArrayList<String>();
        
        try {
            fileReader = new FileReader(filename);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line = null;
            while ((line = bufferedReader.readLine()) != null)
            {
                lines.add(line);
            }   
            bufferedReader.close();
        } catch (FileNotFoundException ex) {
            System.err.println("Error "+ex.getMessage());
        } catch (IOException ex) {
            System.err.println("Error "+ex.getMessage());
        }
        return lines.toArray(new String[lines.size()]);
    }
    
    public String[] readTemplate(String filename) 
    {
        FileReader fileReader = null;
        List<String> lines = new ArrayList<String>();
        
        try {
            fileReader = new FileReader(filename);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line = null;
            while ((line = bufferedReader.readLine()) != null)
            {
                lines.add(line);
            }   
            bufferedReader.close();
            fileReader.close();
        } catch (FileNotFoundException ex) {
            System.err.println("Error "+ex.getMessage());
        } catch (IOException ex) {
            System.err.println("Error "+ex.getMessage());
        }
        return lines.toArray(new String[lines.size()]);
    }
}
