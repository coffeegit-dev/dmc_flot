/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dmc_flot.util;

import java.io.File;

/**
 *
 * @author Sigit Prayitno <sigit.prayitno@service-division.com>
 */
public interface FileMatcher {

    boolean matches(File file);

}
