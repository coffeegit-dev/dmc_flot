/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dmc_flot.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Sigit Prayitno <sigit.prayitno@service-division.com>
 */
public class FileFinder {
    private List<File> files = new ArrayList<>();
    private final FileMatcher matcher;
    private final File root;

    public FileFinder(FileMatcher matcher, File root) {
        this.matcher = matcher;
        this.root = root;
    }

    public List<File> findMatchingFiles() throws IOException {
        files = new ArrayList<>();
        findMatchingFiles(root);
        return files;
    }

    private void findMatchingFiles(File root) throws IOException {
        if (root.isFile()) {
            if (matcher.matches(root)) {
                files.add(root);
            }

            return;
        }

        if (!root.isDirectory()) {
            return;
        }

        for (File file : root.listFiles()) {
            findMatchingFiles(file);
        }
    }
}
