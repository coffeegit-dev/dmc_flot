/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dmc_flot.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.regex.Pattern;

/**
 *
 * @author Sigit Prayitno <sigit.prayitno@service-division.com>
 */
public class Folder {
    public static HashMap<String, ArrayList<String>> dirFiles = new HashMap<String, ArrayList<String>>();

    public static void listFilesForFolder(File folder)
            throws IOException {

        if(folder.isDirectory()) {

            ArrayList<String> fileNames = new ArrayList<String>();

            for (final File fileEntry : folder.listFiles()) {
               // System.out.println(fileEntry.toString());
                if (fileEntry.isDirectory()) {
                //  System.out.println(fileEntry.toString());
                    listFilesForFolder(fileEntry);
                } else {
                    String fileName = (fileEntry.getPath()).toString();
                    fileNames.add(fileEntry.getPath());
                }
            }
            dirFiles.put(folder.getName(), fileNames);
        }
    }
    
    public static String trimFilePath(String fileName) {
        return fileName.substring(fileName.lastIndexOf("/") + 1).substring(fileName.lastIndexOf("\\") + 1);
    }

    public static void main(String[] args) throws IOException {
        boolean isOnlyBatch = false;
        String fileName = "D:\\OUTPUT\\ROOT\\PHS_ALTER_AND_REVIVAL\\PHS_ALTER_AND_REVIVAL_CAPTURE\\04.APRIL\\20180421\\SIANG\\NP_C_420\\001";
        listFilesForFolder(new File(fileName));
        for(Entry<String, ArrayList<String>> foldername : dirFiles.entrySet())
        {
//            System.out.println(foldername.getKey() + " " + foldername.getValue());
            System.out.println(foldername.getKey());
        }
        System.out.println("trim file path:"+trimFilePath(fileName));
        
        String[] batchs = new String[] {"pagi","siang","sore"};
        
        for (String batch : batchs) {
            if(fileName.contains(batch.toUpperCase())){
                File newFileName = null;
                Path path = Paths.get(fileName, batch);
                System.out.println("path:"+path.toString());
                if (path.toFile().exists()) {
                    System.out.println("Real path: "
                        + path.toRealPath(LinkOption.NOFOLLOW_LINKS));
                    newFileName = new File(path.toRealPath(LinkOption.NOFOLLOW_LINKS).toString());
                } else {
                    System.out.println("The file does not exist");
                }
                System.out.println("newFileName:"+newFileName);
            }
        }
        
        // Alternative: use Pattern.quote(File.separator)
        String pattern = Pattern.quote(System.getProperty("file.separator"));
        String[] splittedFileName = fileName.split(pattern);
        
        if(!isOnlyBatch){
            for (String splitted : splittedFileName) {
                if(splitted.equals("pagi".toUpperCase()) || splitted.equals("siang".toUpperCase()) || splitted.equals("sore".toUpperCase())){
                    break;
                }else{
                    System.out.println("splitted:"+splitted);
                }
//                if(!splitted.equals("20180421")){
//                    System.out.println("splitted:"+splitted);
//                }else{
//                    break;
//                }
            }
        }
    }

}
