/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dmc_flot.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

/**
 *
 * @author Sigit Prayitno <sigit.prayitno@service-division.com>
 */
public class QueryModule {
    private static Connection connection;
    private final boolean condition = false;
    
    public QueryModule() {
        this.connection = Config.connect_db();
    }
    
    public Connection getConnection() {
        return connection;
    }
    
    public ResultSet getPrepared(String query){
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage(), "WARNING", JOptionPane.WARNING_MESSAGE);
            System.err.println("Connecting failed, Error:"+ex.getMessage());
        }
        return rs;
    }
    
    public ResultSet getStatement(String query){
        ResultSet rs = null;
        Statement s = null;
        try {
            s = connection.createStatement();
            rs = s.executeQuery(query);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage(), "WARNING", JOptionPane.WARNING_MESSAGE);
            System.err.println("Connecting failed, Error:"+ex.getMessage());
        }
        return rs;
    }
    
    public int execQuery(String query){
        Statement s = null;
        int success = 0;
        try {
            s = connection.createStatement();
            if(s.executeUpdate(query) > 0){
                success = 1;
            }else{
                success = 0;
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage(), "WARNING", JOptionPane.WARNING_MESSAGE);
            System.err.println("Connecting failed, Error:"+ex.getMessage());
        }
        return success;
    }
}
